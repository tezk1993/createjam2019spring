﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorPlantController : MonoBehaviour
{
    private RagdollController _ragdollController;
    private boringDancerTurner _boringDancerTurner;
    private float _ragdollTime;
    public void MonitorFacePlant(RagdollController ragdollController)
    {
        _ragdollController = ragdollController;
        StartCoroutine("FloorProximityCheck");
        _boringDancerTurner = GameObject.FindGameObjectWithTag("GameController").GetComponent<boringDancerTurner>();

    }

    private IEnumerator FloorProximityCheck()
    {
        Debug.Log("Started Floor Proximity Check");
        _ragdollTime = Time.realtimeSinceStartup;

        while (_ragdollController.IsRagdolling)
        {
            /*
            if ((Time.realtimeSinceStartup - _ragdollTime) <= 10f)
            {
                Debug.Log("Hit floor");
                //_ragdollController.ToggleRagdoll();
                _boringDancerTurner.hypeTheDancer(gameObject);
                gameObject.transform.root.gameObject.SetActive(false);
            }
            */
                Debug.Log("Looking for Floor");
            Vector3 direction = Vector3.down;
            if (Physics.Raycast(transform.position, direction, out var hit, 0.3f))
            {
                Debug.Log("Ray Hit " + hit.collider.gameObject.tag);
                if (hit.collider.tag == "Floor")
                {
                    Debug.Log("Hit floor");
                    //_ragdollController.ToggleRagdoll();
                    _boringDancerTurner.hypeTheDancer(gameObject);
                    gameObject.transform.root.gameObject.SetActive(false);
                }
            }
            yield return new WaitForSeconds(0.5f);
        }

        Debug.Log("Coroutine should be stopped");
        yield return null;
    }
}
