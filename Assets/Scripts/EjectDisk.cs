﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjectDisk : MonoBehaviour
{
    public Camera vrCamera;

    [SerializeField]
    private float _headHeight;
    [SerializeField]
    private float _ejectForce = 50f;

    private Rigidbody rb;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    [ContextMenu("Eject")]
    public void Eject()
    {
        //CurrentPosition = transform.localPosition.y;
        _headHeight = vrCamera.transform.position.y;
        Debug.Log("Ya yeet");
        rb.AddForce(Vector3.up * _ejectForce);
        // Start time shift here
        if (transform.position.y < _headHeight)
        {
     
        }
    }
}
