﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DispenserScript : MonoBehaviour
{
    private float _ejectForce = 200f;
    private float _lastEject;
    private SlowMotionController _slowMotionController;
    public GameObject DiskPoolcontainer;
    public List<Rigidbody> DiskPool;
    public Rigidbody EjectedDiskRigidbody;
    private audioPlayer _audioPlayer;
    public List<Texture> vinylTextures;
    private void Start()    
    {
        _audioPlayer = gameObject.GetComponent<audioPlayer>();
        _lastEject = Time.realtimeSinceStartup;
        _slowMotionController = GameObject.FindWithTag("TimeController").GetComponent<SlowMotionController>();

        for (int i = 0; i < DiskPoolcontainer.transform.childCount; i++)
        {
           DiskPool.Add(DiskPoolcontainer.transform.GetChild(i).gameObject.GetComponent<Rigidbody>());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggered with" + other.name);

        if (other.CompareTag("PlayerHand"))
        {
            _audioPlayer.playAudio(0);
            _slowMotionController.StartSlowMotion();
             Eject();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerHand") )
        {
            _lastEject = Time.realtimeSinceStartup;
        }
    }

    public Rigidbody GetPooledObject()
    {
        for (int i = 0; i < DiskPool.Count; i++)
        {
            if (!DiskPool[i].gameObject.activeInHierarchy)
            {
                return DiskPool[i];
            }
        }
        return null;
    }

    [ContextMenu("Eject Disk")]
    public void Eject()
    {
        if (GetPooledObject() && (Time.realtimeSinceStartup - _lastEject) >= 1) { 
            Debug.Log("Ya yeet");
            EjectedDiskRigidbody = GetPooledObject();
            EjectedDiskRigidbody.gameObject.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = vinylTextures[UnityEngine.Random.Range(0,vinylTextures.Count-1)];
            EjectedDiskRigidbody.gameObject.SetActive(true);
            EjectedDiskRigidbody.AddForce(Vector3.up * _ejectForce);
        }
    }
}
