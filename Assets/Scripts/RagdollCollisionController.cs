﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollCollisionController : MonoBehaviour
{
    private audioPlayer _audioPlayer;
    [SerializeField]
    private Rigidbody _rb;
    [SerializeField]
    private RagdollController _ragdollController;
    private float _lastHit;

    private GameController _gameController;

    [SerializeField]
    private float _forceMultiplier;


    bool hit = false;
    public void Awake()
    {
        _audioPlayer = gameObject.transform.root.gameObject.GetComponent<audioPlayer>();
        _rb = GetComponent<Rigidbody>();
        _forceMultiplier = 10000f;
        _ragdollController = gameObject.transform.root.GetComponent<RagdollController>();
        _gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
    }

    private void IsHit(Vector3 directionVector3)
    {
        if (gameObject.transform.root.CompareTag("BoringDancer")) {
            Debug.Log(gameObject.transform.root.name + "Is hit");
            _audioPlayer.playAudio(0);
            _ragdollController.ToggleRagdoll();
            _rb.AddForce(directionVector3 * _forceMultiplier);

            if ((Time.realtimeSinceStartup - _lastHit) >= 1f)
            {
                _lastHit = Time.realtimeSinceStartup;

                if (gameObject.name.Contains("Head"))
                {
                    _gameController.AddPointOnHit(200);
                }
                else
                {
                    _gameController.AddPointOnHit(100);
                }
            }
            hit = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Disk" && hit == false)
        {
            hit = true;

            Debug.Log(collision.gameObject.name);
            IsHit(collision.GetContact(0).normal);


            collision.gameObject.GetComponent<FrisbeeScript>().resetDisk();
        }
    }
}
