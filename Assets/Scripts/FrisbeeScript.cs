﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrisbeeScript : MonoBehaviour
{
    private Rigidbody rb;
    private bool thrown;
    public float curveAmount;

    public Vector3 initPos;
    public Quaternion initRot;

    private audioPlayer _audioPlayer;

    private void Start()
    {
        initPos = gameObject.transform.position;
        initRot = gameObject.transform.rotation;
        _audioPlayer = gameObject.GetComponent<audioPlayer>();
    }

    public void resetDisk()
    {
        _audioPlayer.playAudio(4);

        gameObject.transform.position = initPos;
        gameObject.transform.rotation = initRot;
        gameObject.SetActive(false);

    }

    private void OnDisable()
    {
        StopCoroutine(disableDisc());
    }

    public void startDisableDisc()
    {
        StartCoroutine(disableDisc());
    }

    IEnumerator disableDisc()
    {
        _audioPlayer.playAudio(1);

        yield return new WaitForSeconds(5);
        resetDisk();
    }

    public void pickUpLP()
    {
        _audioPlayer.playAudio(0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision test");
        if(collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Floor"))
        {
            Debug.Log("Hit wall");

            _audioPlayer.playAudio(3);
        }
        if (collision.gameObject.tag.Contains("Dancer"))
        {
            _audioPlayer.playAudio(2);
        }
    }
}
