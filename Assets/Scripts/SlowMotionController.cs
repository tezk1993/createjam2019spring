﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotionController : MonoBehaviour
{
    public float slowDownFactor = 0.005f;
    public float speedUpFactor = 0.05f;
    private AudioSource _audioSource;
    public bool IsSlow;

    void Awake()
    {
        _audioSource = GameObject.FindWithTag("MainSoundTrack").GetComponent<AudioSource>();
    }

    [ContextMenu("Start Slowmotion")]
    public void StartSlowMotion()
    {
        StartCoroutine("HandleSlowDown");
        IsSlow = true;
    }

    [ContextMenu("Stop Slowmotion")]
    public void StopSlowMotion()
    {
        StartCoroutine("HandleSpeedUp");
        IsSlow = false;
    }

    private IEnumerator HandleSpeedUp()
    {
        while (Time.timeScale != 1f)
        {
            Time.timeScale += speedUpFactor;
            Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            yield return new WaitForSeconds(0.01f);
            if (_audioSource.pitch <= 1f)
            {
                _audioSource.pitch += speedUpFactor / 3f;
            }
        }

        _audioSource.pitch = 1f;
        yield return null;
    }

    private IEnumerator HandleSlowDown()
    {
        while (Time.timeScale >=  slowDownFactor * 15f)
        {
            Time.timeScale -= slowDownFactor * 15;
            Time.timeScale = Mathf.Clamp(Time.timeScale, slowDownFactor, 1f);
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            yield return new WaitForSeconds(0.01f);
            if (_audioSource.pitch >= 0.45f)
            {
                _audioSource.pitch -= slowDownFactor * 2.5f;
            }
        }

        _audioSource.pitch = 0.45f;
        yield return null;
    }
}
