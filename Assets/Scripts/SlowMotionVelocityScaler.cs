﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class SlowMotionVelocityScaler : MonoBehaviour
{
    private SlowMotionController _slowMotionController;
    private Throwable _throwable;

    void Awake()
    {
        _slowMotionController = GameObject.FindWithTag("TimeController").GetComponent<SlowMotionController>();
        _throwable = gameObject.GetComponent<Throwable>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_slowMotionController.IsSlow)
        {
            _throwable.scaleReleaseVelocity = 5f;
        }
        else
        {
            _throwable.scaleReleaseVelocity = 4f;
        }
    }
}
