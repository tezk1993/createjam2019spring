﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEditor;
using UnityEngine;


public class RagdollController : MonoBehaviour
{
    [SerializeField]
    private RagdollState _ragdollState;
    private Animator _animator;
    private FloorPlantController _floorPlantController;
    private float _lastSwtich;
    private enum RagdollState
    {
        On,
        Off
    }

    public Collider[] allColliders;

    public bool IsRagdolling;

    private void SetKinematic(bool state)
    {
        Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = state;
        }
    }

    void Awake()
    {
        allColliders = GetComponentsInChildren<Collider>();
        _ragdollState = RagdollState.On;
        _animator = GetComponent<Animator>();
        _floorPlantController = gameObject.transform.root.GetComponentInChildren<FloorPlantController>();
        ToggleRagdoll();
        foreach (Transform t in transform)
        {
            Debug.Log(t.name);
            if (t.gameObject.GetComponent<CapsuleCollider>() != null)
            {
                t.gameObject.GetComponent<CapsuleCollider>().enabled = false;
            }
        }
        colliderToggle(false);
    }

    public void colliderToggle(bool toggleBool)
    {
        for (int i = 0; i < allColliders.Length; i++)
        {
            allColliders[i].enabled = toggleBool;
        }
    }

    [ContextMenu("Toggle Ragdoll")]
    public void ToggleRagdoll()
    {
        Debug.Log("Toggle Ragdoll");
        if ((Time.realtimeSinceStartup - _lastSwtich) <= 1f)
        {
            Debug.Log("Toggle Ragdoll If statement ");
            return;
        }
        switch (_ragdollState)
        {
            case RagdollState.On:
                _lastSwtich = Time.realtimeSinceStartup;
                Debug.Log("Ragdoll on");
                SetKinematic(true);
                _animator.enabled = true;
                _ragdollState = RagdollState.Off;
                IsRagdolling = false;
                break;
            case RagdollState.Off:
                _lastSwtich = Time.realtimeSinceStartup;
                Debug.Log("Ragdoll off");
                SetKinematic(false);
                _animator.enabled = false;
                _ragdollState = RagdollState.On;
                IsRagdolling = true;
                _floorPlantController.MonitorFacePlant(this);
                break;
        }
        

    }


}
