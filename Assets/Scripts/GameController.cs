﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameController : MonoBehaviour
{
    public float TotalScore;
    public TextMeshProUGUI scoreText;
    public int hypeEffectTextAmount;
    private void Start()
    {
        scoreText.text = "Score: " + TotalScore;

    }
    public void AddPointOnHit(int pointAmount)
    {
        TotalScore += pointAmount;
        hypeEffectTextAmount = pointAmount;
        scoreText.text = "Score: " + TotalScore;
    }
}
