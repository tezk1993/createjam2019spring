﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjectButton : MonoBehaviour
{

    public List<DispenserScript> diskDispensers;
    bool ejectCooldown;
    private SlowMotionController _slowMotionController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
     void OnTriggerEnter(Collider other)
    {
        
        Debug.Log("Triggered with" + other.name);


        if (other.CompareTag("PlayerHand") && ejectCooldown == false)
        {
            _slowMotionController.StartSlowMotion();
            ejectCooldown = true;
            for (int i = 0; i < diskDispensers.Count; i++)
            {
                diskDispensers[i].Eject();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("PlayerHand") && ejectCooldown == true)
        {
            ejectCooldown = false;
       
        }
    }
}
