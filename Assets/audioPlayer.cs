﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioPlayer : MonoBehaviour
{

    [System.Serializable]
    public class audioSet{
        
        public List<AudioClip> clips;
        public AudioSource source;
    }

    public List<audioSet> playableAudio;
    // Start is called before the first frame update
   

    public void playAudio(int index)
    {
        Debug.Log("Play audio at index " + index);

        if (playableAudio[index].clips.Count > 0) {
            playableAudio[index].source.clip = playableAudio[index].clips[Random.Range(0, playableAudio[index].clips.Count)];
            playableAudio[index].source.Play();
        }
        else
        {
            playableAudio[index].source.Play();
        }
    }
}
