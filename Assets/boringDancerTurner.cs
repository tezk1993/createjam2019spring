﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class boringDancerTurner : MonoBehaviour
{

    private GameController _gameController;
    public List<GameObject> baseDancers;
    public List<GameObject> boringDancers;
    public List<GameObject> hypeDancers;
    public List<GameObject> hypeEffect;

    public GameObject hypeDancerContainer;
    public GameObject hypeEffectContainer;

    public int selectedDancer;

    public Material boringMat;

    public GameObject newHypeDancer;
    public GameObject newHypeEffect;

    // Start is called before the first frame update
    void Start()
    {
        _gameController = gameObject.GetComponent<GameController>();

        baseDancers.AddRange(GameObject.FindGameObjectsWithTag("Dancer"));
        StartCoroutine(selectNewBoring());


        for (int i = 0; i < hypeDancerContainer.transform.childCount; i++)
        {

            hypeDancers.Add(hypeDancerContainer.transform.GetChild(i).gameObject);

        }
        for (int i = 0; i < hypeEffectContainer.transform.childCount; i++)
        {

            hypeEffect.Add(hypeEffectContainer.transform.GetChild(i).gameObject);

        }
    }
    [ContextMenu("Select Dancer")]
    public void makeBoringDancer()
    {
        selectedDancer = Random.Range(0, baseDancers.Count);
        if (90 > Random.Range(0, 100) && baseDancers.Count> 0)
        {
            Debug.Log(selectedDancer);

            boringDancers.Add(baseDancers[selectedDancer]);
            baseDancers.RemoveAt(selectedDancer);
            Debug.Log(boringDancers[boringDancers.Count-1].transform.GetChild(0).name);

            boringDancers[boringDancers.Count - 1].GetComponent<Animator>().SetTrigger("Angry");

            foreach (Transform t in boringDancers[boringDancers.Count - 1].transform)
            {
                t.gameObject.tag = "BoringDancer";
               

            }
            boringDancers[boringDancers.Count - 1].tag = "BoringDancer";
            boringDancers[boringDancers.Count - 1].GetComponent<RagdollController>().colliderToggle(true);


            Material[] mats = new Material[] { boringMat, boringMat, boringMat };
            boringDancers[boringDancers.Count-1].transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().materials = mats;
        }

    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < hypeDancers.Count; i++)
        {
            if (!hypeDancers[i].gameObject.activeInHierarchy) { 
                return hypeDancers[i];
            }
        }
        return null;
    }
    public GameObject GetpooledEffect()
    {
        for (int i = 0; i < hypeEffect.Count; i++)
        {
            if (!hypeEffect[i].gameObject.activeInHierarchy)
            {
                return hypeEffect[i];
            }
        }
        return null;
    }
    public void hypeTheDancer(GameObject BoringDancer)
    {
        Debug.Log("Get hyped dancer");
        if (GetpooledEffect())
        {
            Debug.Log("Gotten hyped dancer");

            BoringDancer.SetActive(false);
            newHypeEffect = GetpooledEffect();
            newHypeEffect.SetActive(true);
            newHypeEffect.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = _gameController.hypeEffectTextAmount.ToString();

            //_gameController.hypeEffectTextAmount = 0;

            newHypeEffect.transform.position = BoringDancer.transform.position;
            newHypeEffect.transform.position = new Vector3(newHypeEffect.transform.position.x, 1.5f, newHypeEffect.transform.position.z);

        }
        if (GetPooledObject())
        {
            Debug.Log("Gotten hyped dancer");

            newHypeDancer = GetPooledObject();
            newHypeDancer.SetActive(true);
            newHypeDancer.transform.position = BoringDancer.transform.position;
            newHypeDancer.transform.position = new Vector3(newHypeDancer.transform.position.x, 0, newHypeDancer.transform.position.z);

        }
    }

    IEnumerator selectNewBoring()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            makeBoringDancer();
        }
    }
}
